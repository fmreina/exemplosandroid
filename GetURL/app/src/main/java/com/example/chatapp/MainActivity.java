package com.example.chatapp;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class MainActivity extends AppCompatActivity {
    EditText txtMensagem = null;
    ListView mensagens = null;
    ArrayAdapter<String> adapter = null;
    Socket socket = null;
    private final int READ_SOCKET = 1;
    private final int WRITE_SOCKET = 2;

    ListView alertas = null;
    ArrayAdapter<String> adapterAlertas = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        // carrega os componentes de interface em variáveis locais
//        txtMensagem = (EditText)findViewById(R.id.txtMensagem);
//        mensagens = (ListView)findViewById(R.id.mensagens);
        alertas = (ListView)findViewById(R.id.alertas);

        // o adapter contem os dados que serão apresentados na ListView
//        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
//        mensagens.setAdapter(adapter); // atribui os dados para serem apresentados no campo de mensagens

        adapterAlertas = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        alertas.setAdapter(adapterAlertas);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, READ_SOCKET);
        }
        else {
//            waitMessage();
        }
    }


    public void readUrl(){
        setContentView(R.layout.activity_main);

        try {
            AsyncTask<Object, Void, String> x = new Reader().execute(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void btnNoticiasOnClick(View v){
        readUrl();
    }

    public void btnEnviarOnClick(View v){
        sendMessage();
        txtMensagem.setText(null);
    }

    private void waitMessage(){
        try {
//            socket = IO.socket("http://192.168.0.104:3000");
            socket = IO.socket("http://172.20.10.2:3000");
            socket.on("chat message", new Emitter.Listener() {
                @Override
                public void call(final Object... args) {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // coloca a mensagem recebida na lista
                            adapter.add(args[0].toString());
                            adapter.notifyDataSetChanged();

                            // Apenas faz um scroll para o novo item da lista
//                            mensagens.smoothScrollToPosition(adapter.getCount() - 1);
//                            alertas.smoothScrollToPosition(adapter.getCount() - 1);
                        }
                    });
                }
            });
            socket.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void sendMessage(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, WRITE_SOCKET);
        }
        else
            socket.emit("chat message", txtMensagem.getText().toString());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case READ_SOCKET: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    waitMessage();
                    return;
                }
                break;
            }
            case WRITE_SOCKET: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendMessage();
                    return;
                }
                break;
            }
        }

        Toast.makeText(this, "Sem essa permissão o app não irá funcionar. Tente novamente.", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (socket != null)
            socket.disconnect();
    }


    private static class Reader extends AsyncTask<Object, Void, String>{
        MainActivity activity;

        @Override
        protected String doInBackground(Object... params) {
            activity = (MainActivity)params[0];
            try {
                StringBuilder sb = new StringBuilder();
//                URL url = new URL("https://fmreina.bitbucket.io/json/teste.json");
                URL url = new URL("https://fmreina.bitbucket.io/json/ckl.json");

                BufferedReader in;
                in = new BufferedReader(
                        new InputStreamReader(
                                url.openStream()));

                String inputLine;
                while ((inputLine = in.readLine()) != null)
                    sb.append(inputLine);

                in.close();

                return sb.toString();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String str) {
            //Do something with result string

            Gson gson = new Gson();

            Type collectionType = new TypeToken<List<Post>>(){}.getType();
            List<Post> posts = (List<Post>) gson.fromJson( str , collectionType);

            ListView list = activity.findViewById(R.id.alertas);

            for(int i = 0; i<posts.size(); i++){
                activity.adapterAlertas.add(posts.get(i).toString());
                activity.adapterAlertas.notifyDataSetChanged();
                list.setAdapter(activity.adapterAlertas);
                list.smoothScrollToPosition(activity.adapterAlertas.getCount() - 1);
            }

//            System.out.println(str);
//
//            try {
//                JSONObject jsonObj = new JSONObject(str);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//            ListView list = activity.findViewById(R.id.alertas);
//
//            activity.adapterAlertas.add(str == ""? "Erro ao tentar consultar o servidor" : str );
//            activity.adapterAlertas.notifyDataSetChanged();
////            activity.alertas.smoothScrollToPosition(activity.adapterAlertas.getCount() - 1);
//
//            list.setAdapter(activity.adapterAlertas);
//            list.smoothScrollToPosition(activity.adapterAlertas.getCount() - 1);

        }
    }

    public class Post {
        public String title;
        public String website;
        public String authors;
        public String date;
        public String content;
        public Tag[] tags;
        public URL image_url;

        @Override
        public String toString() {
            return title
                    + " \n- by " + authors
                    + " \n- date: " + date
                    + " \n- site: " + website
                    + " \n- content: \n\t" + content
//                    + " \n- tag " + tags
                    + " \n- image: " + image_url;
        }

        public class Tag {
            public int id;
            public String label;
        }
    }

}
